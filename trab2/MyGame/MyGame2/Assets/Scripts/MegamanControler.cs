﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamanControler : MonoBehaviour {

	[Header("Moviment")]
	private float pixelToUnity = 40f;
	public float maxVelocity = 10f;
	public Vector3 moveSpeed = Vector3.zero; //(0,0,0)

	[Header("Animation")]
	public bool isFacingLeft = false;
	public bool isRunning = false;

	[Header("Componets")]
	public Rigidbody2D rigiBody2D;
	public SpriteRenderer spriteRenderer;


	// Use this for initialization
	void Start () {
		rigiBody2D = GetComponent<Rigidbody2D> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		UpdateAnimatorParametrs ();
		HandleHorizontalMovement ();
		HandleVerticalMovement ();
		MoveCharacterController ();
		
	}


	void UpdateAnimatorParametrs(){
	}

	void HandleHorizontalMovement(){
		moveSpeed.x = Input.GetAxis ("Horizontal") * (maxVelocity/pixelToUnity);

		if (Input.GetAxis ("Horizontal") < 0 && !isFacingLeft) {
			//muda o megaman para a esquerda
			isFacingLeft = true;
		} else if (Input.GetAxis ("Horizontal") > 0 && isFacingLeft) {
			//muda o megaman para a direita
			isFacingLeft = false;
		}
		spriteRenderer.flipX = isFacingLeft;
	}

	void HandleVerticalMovement(){
	}

	void MoveCharacterController(){
		rigiBody2D.velocity = moveSpeed;
	}

}
