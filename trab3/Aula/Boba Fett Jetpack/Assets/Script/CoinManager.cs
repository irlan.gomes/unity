﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {

	[Header("References")]
	public ObjectScroll saberPrefab;

	[Header("Saber")]
	public int maxSaber;
	public float sabersOffset;

	private Transform[] saber;
	private int firstSaberIndex;

	private float worldOffset;

	// Use this for initialization
	void Start () {
		saber = new Transform[maxSaber];

		Vector3 saberPosition;

		worldOffset = (
			Camera.main.ViewportToWorldPoint (
				new Vector3(sabersOffset, 0f, 0f)
			) - 
			(Camera.main.ViewportToWorldPoint (
				new Vector3(0f, 0f, 0f)
			))
		).x;

		for (int i = 0; i < maxSaber; i++) {
			// Encontra a posicao de cada 
			saberPosition = Camera.main.ViewportToWorldPoint (
				new Vector3(1f + sabersOffset * i, 
					Random.Range(-0.2f, 0.25f))
			);
			saberPosition.z = 0f;

			// Criar cada saber
			saber [i] = Instantiate(saberPrefab.transform) as Transform;
			saber [i].parent = transform;
			saber [i].localPosition = saberPosition;
		}

		firstSaberIndex = 0;
	}

	// Update is called once per frame
	void Update () {

		Vector3 screenPos = 
			Camera.main.WorldToViewportPoint (
				saber[firstSaberIndex].position
			);

		if (screenPos.x < -0.5f) {
			// Index da posicao anterior
			int lastSaberIndex = 
				(saber.Length + firstSaberIndex - 1) % saber.Length;

			// Posiciona atras do ultimo saber
			saber [firstSaberIndex].localPosition = 
				saber [lastSaberIndex].localPosition +
				new Vector3 (worldOffset, 0f, 0f);

			// Define uma posicao aleatoria no eixo y
			Vector3 saberPos = saber [firstSaberIndex].localPosition;
			saberPos.y = Camera.main.ViewportToWorldPoint (
				new Vector3(0f, Random.Range(-0.2f, 0.5f), 0f)
			).y;
			saber [firstSaberIndex].localPosition = saberPos;

			// Atualiza index do primeiro elemento
			firstSaberIndex = (firstSaberIndex + 1) % saber.Length;
		}
	}
}
