﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScroll : MonoBehaviour {

	public float saberVelocity;

	// Update is called once per frame
	void Update () {
		transform.position += Vector3.left * saberVelocity * Time.deltaTime;
	}

}
