﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float jettPackForce = 75.0f;

	private Rigidbody2D rigidBody2d;

	private Animator animator;

	public ParticleSystem jetpack; 

	private uint coins = 0;

	private bool isDead = false;

	public Texture2D coinIconTexture;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		rigidBody2d = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		if (isDead) {
			return;
		} else {
			Fly ();
		}
	}

	void Fly ()
	{
		bool jetpackActive = Input.GetButton ("Fire1");
		if (jetpackActive) {
			rigidBody2d.AddForce (new Vector2 (0, jettPackForce));

			SetRunAnimation (false);
			AdjustJetpack (true);
		}
	}

	void SetRunAnimation (bool isRun)
	{
		animator.SetBool ("grounded", isRun);
	}

	void SetDieAnimation (bool isDead)
	{
		animator.SetBool ("dead", isDead);
		isDead = true;
	}

	void OnCollisionEnter2D(Collision2D collision) {
		// Se for pipe entao mata o FlappyBird
	
		if (collision.collider.CompareTag ("floor")) {
			SetRunAnimation (true);
			AdjustJetpack (false);
		} else if (collision.collider.CompareTag ("saber")) {
			SetDieAnimation (true);
		} 
			
	}

	void AdjustJetpack (bool jetpackActive)
	{
		jetpack.emissionRate = jetpackActive ? 300.0f : 5.0f; 
	}

	void CollectCoin(Collider2D coinCollider)
	{
		coins++;

		Destroy(coinCollider.gameObject);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag ("coin")) {
			CollectCoin (collider);
		}
	}

	void DisplayCoinsCount()
	{
		Rect coinIconRect = new Rect(10, 10, 32, 32);
		GUI.DrawTexture(coinIconRect, coinIconTexture);                         

		GUIStyle style = new GUIStyle();
		style.fontSize = 30;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.yellow;

		Rect labelRect = new Rect(coinIconRect.xMax, coinIconRect.y, 60, 32);
		GUI.Label(labelRect, coins.ToString(), style);
	}

	void OnGUI()
	{
		DisplayCoinsCount();
	}


}
